Uses https://github.com/inqbus-gmbh/django-compositekey/

geos needs to be installed for geospatial usage in django

## Installation

```
cd /home/bibek/app/virtualenv/
virtualenv openeventmapdjango
source openeventmapdjango/bin/activate
```

Not clone the repository

```
git clone git@bitbucket.org:polous/openeventmap.git
cd openeventmap
pip install -r requirements.txt
```

Copy default settings file

```
cp eventsvisualizer/default.settings.py eventsvisualizer/settings.py
```

Make sure to install GEOS from
[here](https://docs.djangoproject.com/en/1.8/ref/contrib/gis/install/geolibs/)

Now you can start django in your local computer with

```
python manage.py runserver
```


To Update the code in www.openeventmap.tum.de, do

```
cd /path/to/openeventmap-in-local-computer/
git add
git commit
git push
# Login to remote server
ssh username@www.openeventmap.tum.de
cd /opt/myosm/openeventmap
git pull
systemctl restart httpd
```

